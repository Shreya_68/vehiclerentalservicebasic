package VehicalRental.services;

import VehicalRental.models.Branch;
import VehicalRental.models.RentalCity;
import VehicalRental.models.VehicleType;

import java.time.LocalDateTime;
import java.util.List;

public class VehicleRentalService {

    public static void main(String[] args){
        RentalCity rentalCity= new RentalCity("Delhi");
        rentalCity.addBranch("Vasanth Vihar");
        rentalCity.addBranch("Cyber City");
        Branch vasanthVihar= rentalCity.getBranch("Vasanth Vihar");
        Branch cyberCity= rentalCity.getBranch("Cyber City");
        vasanthVihar.allocatePrice(100, VehicleType.SEDAN);
        vasanthVihar.allocatePrice(80,VehicleType.HATCHBACK);
        cyberCity.allocatePrice(200, VehicleType.SEDAN);
        cyberCity.allocatePrice(50, VehicleType.HATCHBACK);
        vasanthVihar.addVehicle("DL 01 MR 9310", VehicleType.SEDAN, "Vasanth Vihar");
        cyberCity.addVehicle("DL 01 MR 9311", VehicleType.SEDAN,"Cyber City");
        cyberCity.addVehicle("DL 01 MR 9312", VehicleType.HATCHBACK, "Cyber City");
        LocalDateTime a = LocalDateTime.of(2020, 2, 29, 10, 00);
        LocalDateTime b = LocalDateTime.of(2020, 2, 29, 13, 00);
        rentalCity.bookVehicle(VehicleType.SEDAN, a, b);
        LocalDateTime c = LocalDateTime.of(2020, 2, 29, 14, 00);
        LocalDateTime d = LocalDateTime.of(2020, 2, 29, 15, 00);
        rentalCity.bookVehicle(VehicleType.SEDAN, c, d);
        rentalCity.bookVehicle(VehicleType.SEDAN, c, d);
        rentalCity.bookVehicle(VehicleType.SEDAN, c, d);
    }
}
