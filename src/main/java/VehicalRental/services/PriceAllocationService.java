package VehicalRental.services;

import VehicalRental.models.Branch;
import VehicalRental.models.RentalCity;
import VehicalRental.models.VehicleType;

public class PriceAllocationService {

    public static void allocatePrice(RentalCity rentalCity, Integer price, VehicleType vehicleType, String branchName){
        Branch addedBranch= rentalCity.getBranch(branchName);
        addedBranch.allocatePrice(price,vehicleType);
    }
}
