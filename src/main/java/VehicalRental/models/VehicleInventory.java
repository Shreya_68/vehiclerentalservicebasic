package VehicalRental.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class VehicleInventory {
    private VehicleType vehicleType;
    List<Vehicle> vehicleList;
    HashMap<String, List<Slot>> bookedSlots;

    public VehicleInventory(VehicleType vehicleType, String vehicleId, String branchName) {
        this.vehicleType = vehicleType;
        this.vehicleList= new ArrayList<>();
        this.vehicleList.add(new Vehicle(vehicleId, vehicleType, branchName));
        this.bookedSlots= new HashMap<>();
    }

    public void addVehicle(VehicleType vehicleType, String vehicleId, String branchName) {
        this.vehicleList.add(new Vehicle(vehicleId, vehicleType, branchName));
    }

    public boolean isAvailable(Vehicle vehicle, LocalDateTime startTime, LocalDateTime endTime) {
        List<Slot> booked = this.bookedSlots.get(vehicle.getId());
        if(booked==null || booked.isEmpty())
            return true;
        for (Slot s : booked) {
            LocalDateTime slotStartTime = s.getStartTime();
            LocalDateTime slotEndTime = s.getEndTime();
            if ((startTime.isAfter(slotStartTime) && startTime.isBefore(slotEndTime)) ||
                    (endTime.isBefore(slotEndTime) && endTime.isAfter(slotStartTime)) ||
                    startTime.isEqual(slotStartTime) || startTime.isEqual(slotEndTime) ||
                    endTime.isEqual(slotEndTime) || endTime.isEqual(slotEndTime))
                return false;
        }
        return true;
    }

    public void bookVehicle(Vehicle vehicle, LocalDateTime startTime, LocalDateTime endTime) {
        String vehicleId = vehicle.getId();
        Slot toBook = new Slot(startTime, endTime);
        List<Slot> slots= new ArrayList<>();
        if (bookedSlots.containsKey(vehicleId)) {
            slots = bookedSlots.get(vehicleId);
            slots.add(toBook);
            bookedSlots.replace(vehicleId, slots);
        } else {
            slots.add(toBook);
            bookedSlots.put(vehicleId, slots);
        }
    }

        // also add cleaup to remove slots after rented

}
