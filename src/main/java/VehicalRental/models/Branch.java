package VehicalRental.models;

import jdk.nashorn.internal.objects.annotations.Getter;

import java.time.LocalDateTime;
import java.util.HashMap;

public class Branch {
    public String branchName;
    private HashMap<VehicleType,VehicleInventory> vehicles;
    private HashMap<VehicleType,Integer> rentalCosts;

    public Branch(String branchName){
        this.branchName=branchName;
        this.vehicles = new HashMap<>();
        this.rentalCosts= new HashMap<>();
    }

    public String getBranchName() {
        return this.branchName;
    }

    public void addVehicle(String vehicleId,VehicleType vehicleType,String branchName){
        if(this.vehicles.containsKey(vehicleType)){
            VehicleInventory vehicleInventory = this.vehicles.get(vehicleType);
            vehicleInventory.addVehicle(vehicleType,vehicleId,branchName);
        }else{
            this.vehicles.put(vehicleType,new VehicleInventory(vehicleType,vehicleId,branchName));
        }
    }

    public void allocatePrice(Integer price,VehicleType vehicleType){
        if(this.rentalCosts.containsKey(vehicleType)){
            this.rentalCosts.replace(vehicleType,price);
        }else{
            this.rentalCosts.put(vehicleType,price);
        }
    }

    public VehicleInventory getVehiclesOfType(VehicleType vehicleType){
        return this.vehicles.getOrDefault(vehicleType,null);
    }

    public Integer getRentalCostOfType(VehicleType vehicleType){
        return this.rentalCosts.getOrDefault(vehicleType,Integer.MAX_VALUE);
    }


}
