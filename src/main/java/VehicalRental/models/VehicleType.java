package VehicalRental.models;

public enum VehicleType {
    SEDAN,
    HATCHBACK,
    SUV
}
