package VehicalRental.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RentalCity {
    private String name;
    private List<Branch> branches;

    public RentalCity(String cityName){
        this.name=cityName;
        this.branches= new ArrayList<Branch>();
    }

    public void addBranch(String branchName){
        this.branches.add(new Branch(branchName));
    }

    public Branch getBranch(String branchName){
        List<Branch> res = this.branches.stream().filter(branch -> branch.branchName.equals(branchName)).collect(Collectors.toList());
        return res.get(0);
    }

    public List<Branch> getAllBranches(){
        return this.branches;
    }

    public void bookVehicle(VehicleType vehicleType, LocalDateTime startTime, LocalDateTime endTime)
            throws NullPointerException {
        try {
            Integer leastCost = Integer.MAX_VALUE;
            Branch bookedBranch = null;
            Vehicle bookedVehicle = null;
            for (Branch branch : branches) {
                VehicleInventory vehicleInventory = branch.getVehiclesOfType(vehicleType);
                List<Vehicle> vehicles = vehicleInventory.vehicleList;
                Integer rentalCost = branch.getRentalCostOfType(vehicleType);
                for (Vehicle v : vehicles) {
                    if (vehicleInventory.isAvailable(v, startTime, endTime) && rentalCost<leastCost) {
                        leastCost = rentalCost;
                        bookedBranch = branch;
                        bookedVehicle = v;
                    }
                }
            }
            if (leastCost == Integer.MAX_VALUE) {
                System.out.println("No "+ vehicleType+ " available at the moment");
            } else {
                bookedBranch.getVehiclesOfType(vehicleType).bookVehicle(bookedVehicle, startTime, endTime);
                System.out.println(bookedVehicle.getId() + " from " + bookedBranch.getBranchName() + " BOOKED.");
            }
        }catch(NullPointerException e){
            System.out.println(e.getMessage());
            throw e;
        }
    }
}
