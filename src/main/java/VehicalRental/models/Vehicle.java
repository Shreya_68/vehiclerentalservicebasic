package VehicalRental.models;

public class Vehicle {
    private String id;
    private String branchName;
    private int rentalCost;
    private VehicleType vehicleType;
    private VehicleStatus vehicleStatus;

    public Vehicle(String vehicleId,VehicleType vehicleType,String branchName) {
        this.id = vehicleId;
        this.vehicleType = vehicleType;
        this.branchName = branchName;
    }

    public boolean isAvailable(){
        if(this.vehicleStatus==VehicleStatus.AVAILABLE)
            return true;
        return false;
    }

    public String getId() {
        return this.id;
    }

    // add more attributes like model etc
}
